import { HttpClient } from "@angular/common/http";
import { Component } from "@angular/core";
import { NgForm } from "@angular/forms";

interface ITasks {
  id: number;
  title: string;
  isComplete: boolean;
}

@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
})
export class AppComponent {
  tasks: ITasks[] = [];
  showMessage: boolean = false;
  showMessageError: string = "";
  showMessageSuccess: string = "";
  places: { region: string; states: string[] }[] = [
    {
      region: "Centro Oeste",
      states: [
        "Distrito Federal",
        "Goias",
        "Mato Grosso",
        "Mato Grosso do Sul",
      ],
    },
    {
      region: "Nordeste",
      states: [
        "Ceará",
        "Alagoas",
        "Bahia",
        "Maranhão",
        "Paraíba",
        "Pernambuco",
        "Piauí",
        "Rio Grande do Norte",
        "Sergipe",
      ],
    },
    {
      region: "Norte",
      states: [
        "Acre",
        "Amapá",
        "Amazonas",
        "Pará",
        "Rondônia",
        "Roraima",
        "Tocantins",
      ],
    },
    {
      region: "Sudeste",
      states: ["Espírito Santo", "Minas Gerais", "Rio de Janeiro", "São Paulo"],
    },
    {
      region: "Sul",
      states: ["Paraná", "Rio Grande do Sul", "Santa Catarina"],
    },
  ];

  regions: any[] = this.places
    .filter((place) => place.region)
    .map((place) => place.region);
  states: any[] = [];
  selected: any;

  // constructor(private backend: BackendService, private logger: Logger) {}
  constructor(private http: HttpClient) {}

  ngOnInit(): void {
    this.getLoadTasks();
  }

  getLoadTasks() {
    this.http
      .get<ITasks>("http://localhost:3000/tasks")
      .subscribe((res: any) => {
        this.tasks = res;
      });
  }

  deleteTasks(id: number) {
    this.http
      .delete(`http://localhost:3000/tasks/${id}`)
      .subscribe((res: any) => {
        // TODO: Exibir mensagem de erro
        this.showMessageError = "Tarefa removida com sucesso!";
      });
  }

  onSubmit(form: NgForm) {
    const body = {
      title: form.value["title"],
      isComplete: false,
    };

    this.http
      .post<ITasks>("http://localhost:3000/tasks", body)
      .subscribe((data) => {
        console.log("data", data);
        this.showMessageSuccess = "Tarefa adicionada com sucesso!";
      });

    this.getLoadTasks();
    form.resetForm();
  }

  onDeleteTask(id: number) {
    let task = this.tasks.filter((x) => x.id === id)[0];
    let index = this.tasks.indexOf(task, 0);
    if (index > -1) {
      this.tasks.splice(index, 1);
      this.deleteTasks(id);
    }
  }

  onCompleteTasks(id: number) {
    let task = this.tasks.filter((x) => x.id === id)[0];
    task.isComplete = true;
  }

  getSelected($event: any) {
    this.selected = $event;
  }

  setStates() {
    let place: any = this.places.filter(
      (place) => place.region == this.selected
    );
    this.states = place.length ? place[0].states : [];
  }
}
