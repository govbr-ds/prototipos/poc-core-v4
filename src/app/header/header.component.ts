import { Component } from "@angular/core";

@Component({
  selector: "br-header",
  templateUrl: "./header.component.html",
})
export class BrHeaderComponent {
  title = "GOVBR-DS -  Quickstart Angular";
  subtitle = "Baseado na v3.0.1 do @govbr-ds/core";

  image = {
    src: "https://www.gov.br/ds/assets/img/govbr-logo.png",
    alt: "logo",
  };

  links = [
    {
      name: "GOVBR-DS",
      href: "https://gov.br/ds",
      title: "Padrão Digital de Governo",
      target: "_blank",
    },
    {
      name: "Serpro",
      href: "https://www.serpro.gov.br/",
      title: "SERPRO",
      target: "_blank",
    },
  ];
}
