## [1.0.2](https://gitlab.com/govbr-ds/dev/govbr-ds-dev-quickstart-angular/compare/v1.0.1...v1.0.2) (2022-09-15)


### Bug Fixes

* **breadcrumb:** responsividade funciona conforme DS ([41040d3](https://gitlab.com/govbr-ds/dev/govbr-ds-dev-quickstart-angular/commit/41040d3802870619cc82cf3b324a07ea3ed18584))

## [1.0.1](https://gitlab.com/govbr-ds/dev/govbr-ds-dev-quickstart-angular/compare/v1.0.0...v1.0.1) (2022-09-14)


### Bug Fixes

* url da imagem do discord ([a0bad51](https://gitlab.com/govbr-ds/dev/govbr-ds-dev-quickstart-angular/commit/a0bad51f8a56a3d0fca81796654c598137a7865a))

## 1.0.0 (2022-09-14)


### Features

* cria quickstart angular usando a v3.0.1 ([24c56ee](https://gitlab.com/govbr-ds/dev/govbr-ds-dev-quickstart-angular/commit/24c56eea58e3d0baf3038618156171bc96808368))
