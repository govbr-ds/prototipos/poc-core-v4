# GOVBR-DS - Quickstart Angular

## Descrição

Projeto exemplificando o reuso dos componentes agnósticos [GOVBR-DS](https://gov.br/ds/ "Padrão Digital de Governo") em um projeto [Angular](https://angular.io/ "Angular").

## Tecnologias

Esse projeto é desenvolvido usando:

1. [Padrão Digital de Governo](https://gov.br/ds/ "Padrão Digital de Governo")
1. [Angular](https://angular.io/ "Angular")

## Dependências

As principais dependências do projeto são:

1. [GOVBR-DS](https://www.gov.br/ds/ "GOVBR-DS")

1. [Font Awesome](https://fontawesome.com/ "Font Awesome")

1. [Fonte Rawline](https://www.cdnfonts.com/rawline.font/ "Fonte Rawline")

1. [Angular](https://angular.io/guide/setup-local "Angular")

> O fontawesome e a fonte rawline podem ser importadas de um CDN. Consulte a documentação no site do [GOVBR-DS](https://www.gov.br/ds/ "GOVBR-DS") para mais detalhes.

## Escopo

Nesse projeto não pretendemos colocar exemplos de todos os componentes. Incluímos alguns componentes para que sirvam de modelo de como reutilizar os estilos, sintaxe e javascript agnósticos disponibilizados no site do design system, de forma que o ciclo de vida do angular não interfira no seu funcionamento.

> Contribuições sobre o uso de outros componentes são bem-vindas e passarão pelo mesmo processo previsto para os outros repositórios.

## Como executar o projeto?

```sh
npm run start
```

Após isso o projeto vai estar disponível no endereço `http://localhost:4200/`.

OBS: Para contribuir com o projeto o clone pode não ser a maneira correta. Por favor consulte os guias sobre como contribuir na nossa [wiki](https://gov.br/ds/wiki/ "Wiki").

### Explicação

Aqui explicamos uma maneira de como fazer uso da biblioteca agnóstica com o Angular reutilizando o maior número de features possível, mas é importante avaliar os requisitos do seu projeto para então tomarem a decisão sobre a arquitetura para ele.

#### Configure o arquivo `angular.json`

Inclua as referências ao GOVBR-DS

```json
    "styles": [
      ...,
      "node_modules/@govbr-ds/core/dist/core.min.css",
      ...
    ]
```

#### declarations.d.ts

Crie um arquivo (no nosso projeto usamos o declarations.d.ts) e declare o módulo dos componentes que deseja usar:

```typescript
declare module "@govbr-ds/core/dist/components/footer/footer";
declare module "@govbr-ds/core/dist/components/header/header";
declare module "@govbr-ds/core/dist/components/menu/menu";
declare module "@govbr-ds/core/dist/components/select/select";
```

#### Componentes

Por favor leve em consideração o ciclo de vida do Angular ao usar o Design System em projetos Angular. Pode ser necessário fazer alterações na estrutura HTML ou chamadas a métodos JS durante momentos específicos do ciclo de vida.

Essa modificações vão variar de componente a componente, versões do Angular e fluxo da aplicação.

## Precisa de ajuda?

> Por favor **não** crie issues para fazer perguntas...

Use nossos canais abaixo para obter tirar suas dúvidas:

-   Site do GOVBR-DS <https://www.gov.br/ds/>

-   Pelo nosso email <govbr-ds@serpro.gov.br>

-   Usando nosso canal no discord <https://discord.gg/U5GwPfqhUP>

## Como contribuir?

Por favor verifique nossos guias de [como contribuir](./CONTRIBUTING.md "Como contribuir?").

### Commits

Nesse projeto usamos um padrão para branches e commits. Por favor observe a documentação na nossa [wiki](https://gov.br/ds/wiki/ "Wiki") para aprender sobre os nossos padrões.

## Créditos

O GOVBR-DS é criado pelo [SERPRO](https://www.serpro.gov.br/ "SERPRO | Serviço Federal de Processamento de Dados") juntamente com a participação da comunidade.

## Licença

Nesse projeto usamos a licença MIT.
